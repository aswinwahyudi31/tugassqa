package com.example.tutorial.service;

import java.util.ArrayList;

import com.example.tutorial.model.Mahasiswa;

public interface IMahasiswaService {
	ArrayList<Mahasiswa> getAllMahasiswa() ;

	Mahasiswa getDetailMahasiswaById(Long id);

	void addMahasiswa(Mahasiswa mahasiswa);

	void updateMahasiswa(Mahasiswa mahasiswa);

	void deleteMahasiswa(Long id);

}
