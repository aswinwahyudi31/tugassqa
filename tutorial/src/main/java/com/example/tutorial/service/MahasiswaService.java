package com.example.tutorial.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.tutorial.dao.MahasiswaRepository;
import com.example.tutorial.model.Mahasiswa;

@Service
public class MahasiswaService implements IMahasiswaService {
	@Autowired
	MahasiswaRepository mahasiswaRepository;
	
	@Override
	public ArrayList<Mahasiswa> getAllMahasiswa() {
		ArrayList<Mahasiswa> all = (ArrayList<Mahasiswa>) mahasiswaRepository.findAll();
		return all;
	}

	@Override
	public Mahasiswa getDetailMahasiswaById(Long id) {
		Mahasiswa mahasiswa = mahasiswaRepository.findById(id);
		return mahasiswa;
	}

	@Override
	public void addMahasiswa(Mahasiswa mahasiswa) {

		mahasiswa.setNim(null);
		mahasiswaRepository.save(mahasiswa);
		
	}

	@Override
	public void updateMahasiswa(Mahasiswa mahasiswa) {
		mahasiswaRepository.save(mahasiswa);
		
	}

	@Override
	public void deleteMahasiswa(Long id) {
		mahasiswaRepository.delete(id);
		
	}

	
	

}
