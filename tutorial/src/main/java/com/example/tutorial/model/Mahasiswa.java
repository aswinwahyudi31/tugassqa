package com.example.tutorial.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name="mahasiswa")
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
public class Mahasiswa {
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "nim")
	public Long nim;
	@Column(name = "nama")
    public String nama;
	@Column(name = "ipk")
    public double ipk;
	@Column(name = "prodi")
    public String prodi;
	public Long getNim() {
		return nim;
	}
	public void setNim(Long nim) {
		this.nim = nim;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public double getIpk() {
		return ipk;
	}
	public void setIpk(double ipk) {
		this.ipk = ipk;
	}
	public String getProdi() {
		return prodi;
	}
	public void setProdi(String prodi) {
		this.prodi = prodi;
	}
	
	
	
}
