package com.example.tutorial.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.tutorial.model.Mahasiswa;
import com.example.tutorial.service.IMahasiswaService;

@RestController
@RequestMapping("/mahasiswa")
public class MahasiswaController {
	@Autowired
	IMahasiswaService ms;
	
	@RequestMapping("/hello")
	String home() {
        return "Hello World!";
    }
	
	@RequestMapping(
			value = "/all",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ArrayList<Mahasiswa> getAll() {
		ArrayList<Mahasiswa> allMahasiswa = ms.getAllMahasiswa();  
		return allMahasiswa;
	}
	
	@RequestMapping(
			value = "/detail/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Mahasiswa> getDetail(@PathVariable("id") final Long id) {
		Mahasiswa mahasiswa = ms.getDetailMahasiswaById(id);  
		return (mahasiswa==null) ? new ResponseEntity<Mahasiswa>(HttpStatus.NOT_FOUND):new ResponseEntity<Mahasiswa>(mahasiswa, HttpStatus.OK);
	}
	
	@RequestMapping(
			value = "/add",
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Mahasiswa> add(@RequestBody Mahasiswa mahasiswa) {
		ms.addMahasiswa(mahasiswa);  
		return new ResponseEntity<Mahasiswa>(mahasiswa, HttpStatus.CREATED);
	}
	
	@RequestMapping(
			value = "/update",
			method = RequestMethod.PUT,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Mahasiswa> update(@RequestBody Mahasiswa mahasiswa) {
		ms.updateMahasiswa(mahasiswa);  
		return new ResponseEntity<Mahasiswa>(mahasiswa, HttpStatus.OK);
	}
	
	@RequestMapping(
			value = "/delete/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Mahasiswa> delete(@PathVariable("id") final Long id) {
		ms.deleteMahasiswa(id);  
		return new ResponseEntity<Mahasiswa>(HttpStatus.OK);
	}
}
