$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("mahasiswa.feature");
formatter.feature({
  "line": 1,
  "name": "Pengelolaan Mahasiswa",
  "description": "",
  "id": "pengelolaan-mahasiswa",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "comments": [
    {
      "line": 4,
      "value": "#\tScenario: Penambahan Data Mahasiswa"
    },
    {
      "line": 5,
      "value": "#Given Ambil jumlah total record saat ini"
    },
    {
      "line": 6,
      "value": "#When Ditambahkan record baru nama\u003dRahul, ipk\u003d3.45, prodi:Informatika"
    },
    {
      "line": 7,
      "value": "#\t    Then Jumlah total data terakhir adalah record awal + 1"
    }
  ],
  "line": 9,
  "name": "Penambahan Data Mahasiswa",
  "description": "",
  "id": "pengelolaan-mahasiswa;penambahan-data-mahasiswa",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 10,
  "name": "Ambil jumlah total record saat ini",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "Ditambahkan record baru nama\u003d\u003cnama\u003e, ipk\u003d\u003cipk\u003e, prodi:\u003cprodi\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "Jumlah total data terakhir adalah record awal + 1",
  "keyword": "Then "
});
formatter.examples({
  "line": 14,
  "name": "",
  "description": "",
  "id": "pengelolaan-mahasiswa;penambahan-data-mahasiswa;",
  "rows": [
    {
      "cells": [
        "nama",
        "ipk",
        "prodi"
      ],
      "line": 15,
      "id": "pengelolaan-mahasiswa;penambahan-data-mahasiswa;;1"
    },
    {
      "cells": [
        "testing1",
        "1.1",
        "Informatika"
      ],
      "line": 16,
      "id": "pengelolaan-mahasiswa;penambahan-data-mahasiswa;;2"
    },
    {
      "cells": [
        "testing2",
        "1.2",
        "Informatika"
      ],
      "line": 17,
      "id": "pengelolaan-mahasiswa;penambahan-data-mahasiswa;;3"
    },
    {
      "cells": [
        "testing3",
        "1.3",
        "Informatika"
      ],
      "line": 18,
      "id": "pengelolaan-mahasiswa;penambahan-data-mahasiswa;;4"
    },
    {
      "cells": [
        "testing4",
        "1.4",
        "Informatika"
      ],
      "line": 19,
      "id": "pengelolaan-mahasiswa;penambahan-data-mahasiswa;;5"
    },
    {
      "cells": [
        "testing5",
        "1.5",
        "Informatika"
      ],
      "line": 20,
      "id": "pengelolaan-mahasiswa;penambahan-data-mahasiswa;;6"
    },
    {
      "cells": [
        "testing6",
        "1.6",
        "Informatika"
      ],
      "line": 21,
      "id": "pengelolaan-mahasiswa;penambahan-data-mahasiswa;;7"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 16,
  "name": "Penambahan Data Mahasiswa",
  "description": "",
  "id": "pengelolaan-mahasiswa;penambahan-data-mahasiswa;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 10,
  "name": "Ambil jumlah total record saat ini",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "Ditambahkan record baru nama\u003dtesting1, ipk\u003d1.1, prodi:Informatika",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "Jumlah total data terakhir adalah record awal + 1",
  "keyword": "Then "
});
formatter.match({
  "location": "MhsSetSteps.given()"
});
formatter.result({
  "duration": 1309017091,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "testing1",
      "offset": 29
    },
    {
      "val": "1.1",
      "offset": 43
    },
    {
      "val": "Informatika",
      "offset": 54
    }
  ],
  "location": "MhsSetSteps.when(String,double,String)"
});
formatter.result({
  "duration": 35126796,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 48
    }
  ],
  "location": "MhsSetSteps.then(int)"
});
formatter.result({
  "duration": 16118748,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "Penambahan Data Mahasiswa",
  "description": "",
  "id": "pengelolaan-mahasiswa;penambahan-data-mahasiswa;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 10,
  "name": "Ambil jumlah total record saat ini",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "Ditambahkan record baru nama\u003dtesting2, ipk\u003d1.2, prodi:Informatika",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "Jumlah total data terakhir adalah record awal + 1",
  "keyword": "Then "
});
formatter.match({
  "location": "MhsSetSteps.given()"
});
formatter.result({
  "duration": 8247649,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "testing2",
      "offset": 29
    },
    {
      "val": "1.2",
      "offset": 43
    },
    {
      "val": "Informatika",
      "offset": 54
    }
  ],
  "location": "MhsSetSteps.when(String,double,String)"
});
formatter.result({
  "duration": 11956547,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 48
    }
  ],
  "location": "MhsSetSteps.then(int)"
});
formatter.result({
  "duration": 8264748,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Penambahan Data Mahasiswa",
  "description": "",
  "id": "pengelolaan-mahasiswa;penambahan-data-mahasiswa;;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 10,
  "name": "Ambil jumlah total record saat ini",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "Ditambahkan record baru nama\u003dtesting3, ipk\u003d1.3, prodi:Informatika",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "Jumlah total data terakhir adalah record awal + 1",
  "keyword": "Then "
});
formatter.match({
  "location": "MhsSetSteps.given()"
});
formatter.result({
  "duration": 7193230,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "testing3",
      "offset": 29
    },
    {
      "val": "1.3",
      "offset": 43
    },
    {
      "val": "Informatika",
      "offset": 54
    }
  ],
  "location": "MhsSetSteps.when(String,double,String)"
});
formatter.result({
  "duration": 8997333,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 48
    }
  ],
  "location": "MhsSetSteps.then(int)"
});
formatter.result({
  "duration": 7431472,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "Penambahan Data Mahasiswa",
  "description": "",
  "id": "pengelolaan-mahasiswa;penambahan-data-mahasiswa;;5",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 10,
  "name": "Ambil jumlah total record saat ini",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "Ditambahkan record baru nama\u003dtesting4, ipk\u003d1.4, prodi:Informatika",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "Jumlah total data terakhir adalah record awal + 1",
  "keyword": "Then "
});
formatter.match({
  "location": "MhsSetSteps.given()"
});
formatter.result({
  "duration": 7136615,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "testing4",
      "offset": 29
    },
    {
      "val": "1.4",
      "offset": 43
    },
    {
      "val": "Informatika",
      "offset": 54
    }
  ],
  "location": "MhsSetSteps.when(String,double,String)"
});
formatter.result({
  "duration": 8954396,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 48
    }
  ],
  "location": "MhsSetSteps.then(int)"
});
formatter.result({
  "duration": 8854844,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": "Penambahan Data Mahasiswa",
  "description": "",
  "id": "pengelolaan-mahasiswa;penambahan-data-mahasiswa;;6",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 10,
  "name": "Ambil jumlah total record saat ini",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "Ditambahkan record baru nama\u003dtesting5, ipk\u003d1.5, prodi:Informatika",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "Jumlah total data terakhir adalah record awal + 1",
  "keyword": "Then "
});
formatter.match({
  "location": "MhsSetSteps.given()"
});
formatter.result({
  "duration": 8664098,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "testing5",
      "offset": 29
    },
    {
      "val": "1.5",
      "offset": 43
    },
    {
      "val": "Informatika",
      "offset": 54
    }
  ],
  "location": "MhsSetSteps.when(String,double,String)"
});
formatter.result({
  "duration": 11459545,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 48
    }
  ],
  "location": "MhsSetSteps.then(int)"
});
formatter.result({
  "duration": 9030770,
  "status": "passed"
});
formatter.scenario({
  "line": 21,
  "name": "Penambahan Data Mahasiswa",
  "description": "",
  "id": "pengelolaan-mahasiswa;penambahan-data-mahasiswa;;7",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 10,
  "name": "Ambil jumlah total record saat ini",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "Ditambahkan record baru nama\u003dtesting6, ipk\u003d1.6, prodi:Informatika",
  "matchedColumns": [
    0,
    1,
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "Jumlah total data terakhir adalah record awal + 1",
  "keyword": "Then "
});
formatter.match({
  "location": "MhsSetSteps.given()"
});
formatter.result({
  "duration": 8808867,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "testing6",
      "offset": 29
    },
    {
      "val": "1.6",
      "offset": 43
    },
    {
      "val": "Informatika",
      "offset": 54
    }
  ],
  "location": "MhsSetSteps.when(String,double,String)"
});
formatter.result({
  "duration": 8506790,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "1",
      "offset": 48
    }
  ],
  "location": "MhsSetSteps.then(int)"
});
formatter.result({
  "duration": 8137079,
  "status": "passed"
});
});