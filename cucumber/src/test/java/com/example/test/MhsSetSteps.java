package com.example.test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.json.JSONArray;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MhsSetSteps {
	
	int jmlrec;
	
	@Given("^Ambil jumlah total record saat ini$")
	public void given() throws Throwable{
		HttpResponse<JsonNode> jsonResponse = Unirest.get("http://localhost:8080/mahasiswa/all")
				.header("Accept", "application/json")
				.asJson();
		JSONArray myArray = jsonResponse.getBody().getArray();
		
		jmlrec = myArray.length();
	}
	
	@When("^Ditambahkan record baru nama=(.+), ipk=(\\d\\.\\d+), prodi:(.+)$")
	public void when(String nama, double ipk, String prodi) throws Throwable{
		/*
		 {
      
      		"nama": "anjeli",
      		"ipk": 1.3,
      		"prodi": "Informatika"
   		}
		 
		 */
		
		StringBuilder jsonStr = new StringBuilder();
		jsonStr.append("{\"nama\": \"" + nama + "\",").
				append("\"ipk\": " + ipk + ",").
				append("\"prodi\": \"" + prodi + "\"").
				append("}");
		
		@SuppressWarnings("unused")
		HttpResponse<JsonNode> jsonResponse = Unirest.post("http://localhost:8080/mahasiswa/add")
				.header("Content-Type", "application/json")
				.header("Accept", "application/json")
				.body(jsonStr.toString())
				.asJson();
	}

	@Then("^Jumlah total data terakhir adalah record awal \\+ (\\d+)$")
	public void then(int jumlah) throws Throwable{
		HttpResponse<JsonNode> jsonResponse = Unirest.get("http://localhost:8080/mahasiswa/all")
				.header("Accept", "application/json")
				.asJson();
		
		JSONArray myArray = jsonResponse.getBody().getArray();
		
		assertThat(myArray.length(), equalTo(jmlrec+jumlah));
		
		
	}

}
