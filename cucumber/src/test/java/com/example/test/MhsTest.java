package com.example.test;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 *
 * @author budsus
 */
@RunWith(Cucumber.class)
@CucumberOptions(
		monochrome = true,
		plugin = {"pretty", "html:target/cucumber"},
        features = "src/test/resources",
        glue = "com.example.test"
)
public class MhsTest {

}