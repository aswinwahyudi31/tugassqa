Feature: Pengelolaan Mahasiswa

    
#	Scenario: Penambahan Data Mahasiswa
      #Given Ambil jumlah total record saat ini
      #When Ditambahkan record baru nama=Rahul, ipk=3.45, prodi:Informatika
#	    Then Jumlah total data terakhir adalah record awal + 1
	    
	Scenario Outline: Penambahan Data Mahasiswa
			Given Ambil jumlah total record saat ini
      When Ditambahkan record baru nama=<nama>, ipk=<ipk>, prodi:<prodi>
	    Then Jumlah total data terakhir adalah record awal + 1
	    
	Examples:
	|nama| ipk| prodi|
	|testing1|1.1|Informatika|
	|testing2|1.2|Informatika|
	|testing3|1.3|Informatika|
	|testing4|1.4|Informatika|
	|testing5|1.5|Informatika|
	|testing6|1.6|Informatika|    